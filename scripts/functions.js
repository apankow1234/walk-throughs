/**
 * Summary.
 * 
 * Description.
 * 
 * @link    www.andrewpankow.com
 * @file    This is where the JavaScript functions will be kept for use across the entire website.
 * @summary The website's general use JavaScript functions
 * @author  Andrew Pankow, DRA, MFA <andrew@andrewpankow.com>
 */