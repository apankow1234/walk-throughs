// quick css parser for markup
// --------------------------------------------------------
// collapse all extra whitespace
// create new CSS Entry object

// create new CSS Selector object
//  * `ELEMENT`        =>  `html element`
//  * `#ID`            =>  `id`
//  * `.CLASS`         =>  `class`
//  * `*`              =>  `css selector for any html element`
//  * `,`              =>  `css selector end AND css selector prompt`
//  * `>`              =>  `direct child`
//  * `+`              =>  `very next sibling`
//  * `~`              =>  `any following sibling`
//  * `:KEYWORD`       =>  `status`
//  * `::KEYWORD`      =>  `pseudo-element`
//  * `[ATTR]`         =>  `html attribute`
//  * `[ATTR =  VAL]`  =>  `html attribute equalling html attribute value VAL`
//  * `[ATTR |= VAL]`  =>  `html attribute beginning with an html attribute value VAL`
//  * `[ATTR ^= VAL]`  =>  `html attribute with html attribute value starting with VAL`
//  * `[ATTR $= VAL]`  =>  `html attribute with html attribute value ending with VAL`
//  * `[ATTR ~= VAL]`  =>  `html attribute with html attribute value containing the word VAL`
//  * `[ATTR *= VAL]`  =>  `html attribute with html attribute value containing substring VAL`
//  * `{`              =>  `css selector end AND css attributes container begin`

// create series of CSS Attribute objects
//  * `:`              =>  `css attribute and css attribute value separator`
//  * `;`              =>  `css attribute's end`
//  * `}`              =>  `css attributes container's end (AND css final-attribute-in-container's end, if no semicolon)`
//  * `ATTR : VAL`     =>  `css attribute with it's value`
//  * `ATTR : VAL VAL` =>  `css attribute with it's space-separated values`

class cssSpecificityGroup {
	constructor( specificityString ) {
		this.next = null;
		this.specific = specificityString.replace(/(^\s|\s$)/g, '');
		this.values = [];
		let m;
		let re = /([\[](.*?)[\]]|(.[^\s]*?)[\(](.*?)[\)]|(?:[\.\#\:]|\:\:)?\w+|[\>\+\~\*])/g;
		// let re = /([\[](.*?)[\]]|[ \>\+\~])/g;
		let re2 = /([\>\+\~\$]?\=)/g;
		do {
			if( m = re.exec( specificityString ) ) {
				console.log( m );
				if( m[2] ) {
					let n = re2.exec( m[2] );
					let attr = m[2].slice(0,n.index);
					let sep  = n[1];
					let value = m[2].slice(n.index+sep.length,m[2].length);
					console.log( attr, sep, value );
				} else {
					console.log( m[1] );
				}
			}
		} while( m );
	}
}

class cssSelector {
	constructor( selectorString ) {
		this.sel = selectorString.replace(/\s+/g, ' ').replace(/(^\s|\s$)/g, '');
		let val = this.sel.split(/\s/g);
		this.specifics = [];
		for(let i=0; i<val.length; i++) {
			// console.log( val[i] );
			this.specifics.push( new cssSpecificityGroup( val[i] ) );
		}
	}
}

class cssValueGroup {
	constructor() {
		this.values = [];
		this.use = true;
	}

	addValue( cssAttributeValueString ) {
		if( cssAttributeValueString != undefined && cssAttributeValueString != "undefined" )
			this.values.push( cssAttributeValueString );
		return this;
	}

	toggleUse() {
		this.use = this.use ? false : true;
		return this;
	}
}

class cssAttribute {
	constructor( attributeString ) {
		let attr = attributeString.split(':')[0].replace(/\s*/,'');
		if( attr ) {
			let attrValRegex = /(((\S+)[\(](.*?)[\)]|([\d\.]+)([\w]{1,3}|[\%])?|([\-\w]+))[\,]?)/igm;
			this.attr = attr;
			this.valueGroups = [];
			let vals = attributeString.replace(/[\;]$/,'').split(':')[1];
			let m;
			let valGroup = new cssValueGroup();
			do {
				if( m = attrValRegex.exec( vals ) ) {
					valGroup.addValue( m[2] );
					if( /\,$/igm.exec( m[1] ) != null ) {
						this.valueGroups.push( valGroup );
						valGroup = new cssValueGroup();
					}
				}
			} while( m );
			this.valueGroups.push( valGroup );
		}
	}
}

class cssEntry {
	constructor( cssString ) {
		this.attributes = {};
		this.selectors = {};

		if(cssString != undefined)
			this.parse( cssString );
	}

	addAttribute( attributeString ) {
		if( attributeString != undefined ) {
			let newAttr = new cssAttribute( attributeString.replace(/(^\s|\s$)/g, '') );
			if( newAttr.attr ) {
				if( this.attributes.hasOwnProperty( newAttr.attr ) ) {
					this.attributes[ newAttr.attr ].valueGroups.push( newAttr.valueGroups[0] );
				} else {
					this.attributes[ newAttr.attr ] = newAttr;
				}
			}
		}
	}	

	addSelector( selectorString ) {
		if( selectorString != undefined ) {
			let newSel = new cssSelector( selectorString.replace(/(^\s|\s$)/g, '') );

			if( !this.selectors.hasOwnProperty( newSel.sel ) ) {
				this.selectors[ newSel.sel ] = newSel;
			}
		}
	}

	parse( cssString ) {
		let stripped      = cssString.replace(/\s+/g,' ');
		stripped          = stripped.replace(/[\/][\*].*?[\*][\/]/g,'');
		let entryParts    = /(.*?)[\{]\s*(.*?)\s*[\}]/igm.exec( stripped );

		let selectorsRaw  = entryParts[1].split(',');
		let attributesRaw = entryParts[2].split(';');

		for( let i=0; i<selectorsRaw.length; i++ ) {
			let selectorString = selectorsRaw[i].replace(/\s+/g,' ');
			this.addSelector( selectorString );
		}

		for( let i=0; i<attributesRaw.length; i++ ) {
			let attributeString = attributesRaw[i].replace(/\s+/g,' ');
			this.addAttribute( attributeString );
		}
	}
}






let testEntries = [
	`nav.collapsible > .menu > *
		{
			transition: 0.25s;  /* better UX if things animate in and out rather than blink       */
			opacity: 0;         /* makes contents transparent                                     */
			font-size: 0em;     /* makes contents take up 0 (zero/ no) space on the page          */
		}`,

	`.effect6
		{
		  	position:relative;
		    -webkit-box-shadow:0 1px 4px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1) inset;
		       -moz-box-shadow:0 1px 4px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1) inset;
		            box-shadow:0 1px 4px rgba(0, 0, 0, 0.3), 0 0 40px rgba(0, 0, 0, 0.1) inset;
		}`,
	`.effect7:before, .effect7:after
		{
			content:"";
		    position:absolute;
		    z-index:-1;
		    -webkit-box-shadow:0 0 20px rgba(0,0,0,0.8);
		    -moz-box-shadow:0 0 20px rgba(0,0,0,0.8);
		    box-shadow:0 0 20px rgba(0,0,0,0.8);
		    top:0;
		    bottom:0;
		    left:10px;
		    right:10px;
		    -moz-border-radius:100px / 10px;
		    border-radius:100px / 10px;
		}`,
	`a[href$=".jpg"]
		{
			background: url(jpeg.gif) no-repeat left 50%;
			padding: 2px 0 2px 20px;
		}`,
	`:lang(fr) > a#flag {
			background-image: url(french.gif);
		}`
];

let tests = [];
for( e in testEntries ) {
	let test = new cssEntry( testEntries[e] );
	tests.push( test );
	console.log(test);
}