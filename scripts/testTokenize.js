class CSSToken {
	constructor() {
		this.root = true;
	}

	get tokenTree() {
		let tree = [];

		let node = this.constructor;
		while( node.name ) {
			tree.push( node.name );
			node = node.__proto__;
		}
		return tree;
	}
}
// TODO(andrew):	CSSURLToken
// TODO(andrew):	CSSURLUnquotedToken
// TODO(andrew):	CSSDeclarationToken
// TODO(andrew):	CSSFunctionBlockToken
// TODO(andrew):	CSSQualifiedRuleToken
// TODO(andrew):	CSSAtRuleToken

class CSSEOFToken extends CSSToken {
	constructor( tokenValue ) {
		super();
	}
}
class CSSComponentValueToken extends CSSToken {
	constructor( tokenValue ) {
		super();
		this.val = tokenValue;
	}

	get length() {
		return this.val.length;
	}
}
class CSSPreservedToken extends CSSComponentValueToken {
	constructor( tokenValue ) {
		super( tokenValue );
	}
}
class CSSComplexToken extends CSSComponentValueToken  {
	constructor() {
		let value = [ ...arguments ].map( (token)=>{ 
			token.root = false;
			return token ? token.val : ""; 
		} ).join('');
		super( value );
		this.contents = [...arguments];
	}

	get length() {
		return this.contents.length;
	}
}
class CSSBlockToken extends CSSComplexToken  {
	constructor() {
		super( ...arguments );
		this.contents = arguments.length > 2 ? [...arguments].slice( 1, arguments.length-1 ) : [];
		this.open = arguments[0];
		this.close = arguments[ arguments.length-1 ];
	}

	get length() {
		return 2 + this.contents.length;
	}
}

class CSSDimensionToken extends CSSComplexToken  {
	constructor( numberToken, identToken ) {
		super( numberToken, identToken );
		this.number = numberToken;
		this.unit   = identToken;
	}
}
class CSSPercentageToken extends CSSDimensionToken  {
	constructor( numberToken, percentToken ) {
		super( numberToken, percentToken );
		this.unit = percentToken;
	}
}
class CSSAtKeywordToken extends CSSPreservedToken  {
	constructor( tokenValue ) {
		super( CSSAtKeywordToken.prefix + tokenValue );
		this.name = tokenValue;
		this.prefix = CSSAtKeywordToken.prefix;
	}
}
CSSAtKeywordToken.prefix = `\@`;
class CSSDelimiterToken extends CSSPreservedToken  {
	constructor( tokenValue ) {
		super( tokenValue );
	}
}
class CSSGroupingDelimiterToken extends CSSDelimiterToken {
	constructor( tokenValue ) {
		super( tokenValue );
	}
}
class CSSCDATAOpenToken extends CSSGroupingDelimiterToken  {
	constructor() {
		super( ...arguments || CSSCDATAOpenToken.ident );
	}

	get length() {
		return 4;
	}
}
CSSCDATAOpenToken.ident = `\<\!\-\-`;
CSSCDATAOpenToken.re = /([\<]\s*[\!]\s*[\-]\s*[\-])/;
class CSSCDATACloseToken extends CSSGroupingDelimiterToken  {
	constructor() {
		super( ...arguments || CSSCDATACloseToken.ident );
	}

	get length() {
		return 3;
	}
}
CSSCDATACloseToken.ident = `\-\-\>`;
CSSCDATACloseToken.re = /([\-]\s*[\-]\s*[\>])/;
class CSSParenOpenToken extends CSSGroupingDelimiterToken  {
	constructor() {
		super( CSSParenOpenToken.ident );
	}

	get length() {
		return 1;
	}
}
CSSParenOpenToken.ident = `\(`;
class CSSParenCloseToken extends CSSGroupingDelimiterToken  {
	constructor() {
		super( CSSParenCloseToken.ident );
	}

	get length() {
		return 1;
	}
}
CSSParenCloseToken.ident = `\)`;
class CSSBraceOpenToken extends CSSGroupingDelimiterToken  {
	constructor() {
		super( CSSBraceOpenToken.ident );
	}

	get length() {
		return 1;
	}
}
CSSBraceOpenToken.ident = `\{`;
class CSSBraceCloseToken extends CSSGroupingDelimiterToken  {
	constructor() {
		super( CSSBraceCloseToken.ident );
	}

	get length() {
		return 1;
	}
}
CSSBraceCloseToken.ident = `\}`;
class CSSBracketOpenToken extends CSSGroupingDelimiterToken  {
	constructor() {
		super( CSSBracketOpenToken.ident );
	}

	get length() {
		return 1;
	}
}
CSSBracketOpenToken.ident = `\[`;
class CSSBracketCloseToken extends CSSGroupingDelimiterToken  {
	constructor() {
		super( CSSBracketCloseToken.ident );
	}

	get length() {
		return 1;
	}
}
CSSBracketCloseToken.ident = `\]`;
class CSSCommentOpenToken extends CSSGroupingDelimiterToken  {
	constructor() {
		super( CSSCommentOpenToken.ident );
	}

	get length() {
		return 2;
	}
}
CSSCommentOpenToken.ident = `\/\*`;
class CSSCommentCloseToken extends CSSGroupingDelimiterToken  {
	constructor() {
		super( CSSCommentCloseToken.ident );
	}

	get length() {
		return 2;
	}
}
CSSCommentCloseToken.ident = `\*\/`;
class CSSQuoteToken extends CSSGroupingDelimiterToken  {
	constructor() {
		super( CSSQuoteToken.ident );
	}

	get length() {
		return 1;
	}
}
CSSQuoteToken.ident = `\"`;
class CSSApostropheToken extends CSSGroupingDelimiterToken  {
	constructor() {
		super( CSSApostropheToken.ident );
	}

	get length() {
		return 1;
	}
}
CSSApostropheToken.ident = `\'`;
class CSSExclamationPointToken extends CSSDelimiterToken  {
	constructor() {
		super( CSSExclamationPointToken.ident );
	}

	get length() {
		return 1;
	}
}
CSSExclamationPointToken.ident = `\!`;
class CSSIdentToken extends CSSPreservedToken  {
	constructor( tokenValue ) {
		super( tokenValue );
	}
}
CSSIdentToken.re = /([\-]*(?:[a-zA-Z\_]|[^\x00-\x7F]|[\\](?:[^0-9a-fA-F\r\n\f]|[0-9a-fA-F]{6}))(?:[a-zA-Z0-9\_\-]|[^\x00-\x7F]|[\\](?:[^0-9a-fA-F\r\n\f]|[0-9a-fA-F]{6}))*)/;
class CSSImportantToken extends CSSComplexToken  {
	constructor() {
		let ident = arguments.length > 0 ? 
			[...arguments] : 
			new Array(new CSSExclamationPointToken(), new CSSIdentToken('important'));
		super( ...ident );
		this.contents = ident;
	}
}
CSSImportantToken.ident = `\!important`;
class CSSNumberToken extends CSSPreservedToken  {
	constructor( tokenValue ) {
		super( tokenValue );
		this.flag = '';
		if( !tokenValue.match(/[eE\+]/mg) ) {
			if( tokenValue.match(/\./mg) ) {
				this.val = parseFloat( this.val );
				this.flag = 'n';
			} else {
				this.val = parseInt( this.val );
				this.flag = 'i';
			}
		} else {
			// https://www.w3.org/TR/css-syntax-3/#convert-a-string-to-a-number
			// s·(i + f·10^(-d))·10^(te)
			this.val = parseFloat( this.val );
			this.flag = 'n';
		}
	}

	get length() {
		return (this.val+'').length;
	}
}
CSSNumberToken.re = /((?:\d+(?:[\.]\d*)?|[\.]\d+)(?:[eE][\+\-]?\d+)?)/;
class CSSCommentToken extends CSSBlockToken  {
	constructor() {
		super( ...arguments );
	}
}
CSSCommentToken.open  = CSSCommentOpenToken;
CSSCommentToken.close = CSSCommentCloseToken;
class CSSCDATAToken extends CSSBlockToken  {
	constructor() {
		super( ...arguments );
	}
}
CSSCDATAToken.open  = CSSCDATAOpenToken;
CSSCDATAToken.close = CSSCDATACloseToken;
class CSSWhitepaceToken extends CSSDelimiterToken {
	constructor() {
		super( CSSWhitepaceToken.ident );
		this.numSpaces = 1;
	}

	setNumSpaces( numSpaces = 2 ) {
		this.numSpaces = numSpaces;
		// this.val = (new Array( this.numSpaces + 1 ).join(" "));
		return this;	
	}

	get length() {
		return this.numSpaces;
	}
}
CSSWhitepaceToken.ident = ` `;
CSSWhitepaceToken.re = /([ \t]+)/;
class CSSNewLineToken extends CSSWhitepaceToken {
	constructor() {
		super( CSSNewLineToken.ident );
	}

	setNumSpaces( numSpaces = 2 ) {
		this.numSpaces = numSpaces;
		// this.val = (new Array( this.numSpaces + 1 ).join("\r\n"));
		return this;
	}
}
CSSNewLineToken.ident = `\r\n`;
CSSNewLineToken.re = /((?:[\n\f\r]|\r\n)+)/;
class CSSColonToken extends CSSDelimiterToken {
	constructor() {
		super( CSSColonToken.ident );
	}

	get length() {
		return 1;
	}
}
CSSColonToken.ident = `\:`;
class CSSSemicolonToken extends CSSDelimiterToken {
	constructor() {
		super( CSSSemicolonToken.ident );
	}

	get length() {
		return 1;
	}
}
CSSSemicolonToken.ident = `\;`;
class CSSCommaToken extends CSSDelimiterToken {
	constructor() {
		super( CSSSemicolonToken.ident );
	}

	get length() {
		return 1;
	}
}
CSSSemicolonToken.ident = `\,`;
class CSSPercentToken extends CSSDelimiterToken {
	constructor() {
		super( CSSPercentToken.ident );
	}

	get length() {
		return 1;
	}
}
CSSPercentToken.ident = `\%`;
class CSSMatcherToken extends CSSDelimiterToken {
	constructor( tokenValue ) {
		super( tokenValue );
		this.matcherSymbol = CSSMatcherToken.matcherSymbol;
		this.prefix        = CSSMatcherToken.prefix;
	}
}
CSSMatcherToken.matcherSymbol = `\=`;
CSSMatcherToken.prefix        = ``;
class CSSSuffixMatcherToken extends CSSMatcherToken {
	constructor() {
		super( CSSSuffixMatcherToken.ident );
		this.prefix = CSSSuffixMatcherToken.prefix;
	}

	get length() {
		return 2;
	}
}
CSSSuffixMatcherToken.prefix = `\$`;
CSSSuffixMatcherToken.ident  = `\$\=`;
class CSSSubstringMatcherToken extends CSSMatcherToken {
	constructor() {
		super( CSSSubstringMatcherToken.ident );
		this.prefix = CSSSubstringMatcherToken.prefix;
	}

	get length() {
		return 2;
	}
}
CSSSubstringMatcherToken.prefix = `\*`;
CSSSubstringMatcherToken.ident  = `\*\=`;
class CSSPrefixMatcherToken extends CSSMatcherToken {
	constructor() {
		super( CSSPrefixMatcherToken.ident );
		this.prefix = CSSPrefixMatcherToken.prefix;
	}

	get length() {
		return 2;
	}
}
CSSPrefixMatcherToken.prefix = `\^`;
CSSPrefixMatcherToken.ident  = `\^\=`;
class CSSIncludeMatcherToken extends CSSMatcherToken {
	constructor() {
		super( CSSIncludeMatcherToken.ident );
		this.prefix = CSSIncludeMatcherToken.prefix;
	}

	get length() {
		return 2;
	}
}
CSSIncludeMatcherToken.prefix = `\~`;
CSSIncludeMatcherToken.ident  = `\~\=`;
class CSSDashMatcherToken extends CSSMatcherToken {
	constructor() {
		super( CSSDashMatcherToken.ident );
		this.prefix = CSSDashMatcherToken.prefix;
	}

	get length() {
		return 2;
	}
}
CSSIncludeMatcherToken.prefix = `\|`;
CSSDashMatcherToken.ident     = `\|\=`;
class CSSColumnToken extends CSSPreservedToken {
	constructor() {
		super( CSSColumnToken.ident );
	}

	get length() {
		return 2;
	}
}
CSSColumnToken.ident = `\|\|`;
class CSSStringToken extends CSSBlockToken {
	constructor() {
		super( ...arguments );
		this.bad  = false;
		this.char = arguments[0];
		this.validate();
	}

	validate() {
		let nl = -1;
		for( let i=0; i < this.contents.length; i++ ) {
			if( this.contents[i].constructor.name == `CSSNewLineToken` 
				&& this.contents[i-1].val != `\\`) {
				this.bad = true;
			}
		}
		return this.bad;
	}
}

class CSSBraceBlockToken extends CSSBlockToken {
	constructor() {
		super( ...arguments );
	}
}
CSSBraceBlockToken.open  = CSSBraceOpenToken;
CSSBraceBlockToken.close = CSSBraceCloseToken;

class CSSBracketBlockToken extends CSSBlockToken {
	constructor() {
		super( ...arguments );
	}
}
CSSBracketBlockToken.open  = CSSBracketOpenToken;
CSSBracketBlockToken.close = CSSBracketCloseToken;

class CSSParenBlockToken extends CSSBlockToken {
	constructor() {
		super( ...arguments );
	}
}
CSSParenBlockToken.open  = CSSParenOpenToken;
CSSParenBlockToken.close = CSSParenCloseToken;

class CSSHashToken extends CSSPreservedToken {
	constructor( tokenValue ) {
		super( CSSHashToken.prefix + tokenValue );
		this.name   = tokenValue;
		this.prefix = CSSHashToken.prefix;
		this.flag   = CSSHashToken.FLAGS.UNRESTRICTED;
	}
}
CSSHashToken.prefix = `\#`;
CSSHashToken.re     = /[\#]((?:[a-zA-Z0-9\_\-]|[^\x00-\x7F]|[\\](?:[^0-9a-fA-F\r\n\f]|[0-9a-fA-F]{6}))*)/;
CSSHashToken.FLAGS  = {
	'UNRESTRICTED' : 0,
	'ID'           : 1
}
class CSSEscapeToken extends CSSPreservedToken {
	constructor( tokenValue ) {
		super( tokenValue );
	}
}
CSSEscapeToken.re = /([\\](?:[^0-9a-fA-F\r\n\f]|[0-9a-fA-F]{6}))/;
class CSSUnicodeRangeToken extends CSSPreservedToken {
	constructor( tokenValue ) {
		super( tokenValue );
	}
}
CSSUnicodeRangeToken.re = /[uU][\+]((?:[0-9a-fA-F]{1,6}[\-][0-9a-fA-F]{1,6}|[0-9a-fA-F\?]{1,5}[\?]|[0-9a-fA-F]{1,6}))/;






class CSSParser {

	constructor( cssString = `` ) {
		this.string    = cssString;
		this.tokens    = [];

		if( cssString )
			this.tokens = CSSParser.tokenize( this.string );
	}

	static group( tokens = [] ) {
			let onlyGroupDelims = tokens.filter(e=>{ return ( e instanceof CSSGroupingDelimiterToken ); });
			for( let i=0; i < onlyGroupDelims.length; i++ ) {
				let c = onlyGroupDelims[i];
				let n = onlyGroupDelims[i+1];
				for( let j=0; j < CSSParser.GroupTypes.length; j++ ) {
					let g = CSSParser.GroupTypes[j];
					if( g.open.name == c.constructor.name && g.close.name == n.constructor.name ) {
						let beginning = tokens.indexOf( c );
						let ending    = tokens.indexOf( n ) + 1;
						let grouping  = new g( ...tokens.slice( beginning, ending ) );
						tokens.splice( beginning, grouping.length, grouping );
						CSSParser.group( tokens );
					}
				}
			}
			return tokens;
		}

	static tokenize( cssString = ``, rawOnly = false ) {
		let tokens  = [];

		/*** PASS 1 - Raw Tokens: https://www.w3.org/TR/css-syntax-3/#consume-a-token ***/
		for ( let i=0; i < cssString.length; i++ ) {
			let newToken;
			let remaining = cssString.substr(i);
			let c         = cssString[i];
			let n         = cssString[i+1];
			let cn        = c+n;

			let wsString          = remaining.match( CSSWhitepaceToken.re    );
			let nlString          = remaining.match( CSSNewLineToken.re      );
			let numString         = remaining.match( CSSNumberToken.re       );
			let hashIdentString   = remaining.match( CSSHashToken.re         );
			let identString       = remaining.match( CSSIdentToken.re        );
			let cdoString         = remaining.match( CSSCDATAOpenToken.re    );
			let cdcString         = remaining.match( CSSCDATACloseToken.re   );
			let escapeString      = remaining.match( CSSEscapeToken.re       );
			let unicodeString     = remaining.match( CSSUnicodeRangeToken.re );

			if( c.match(/\s/) ) { // whitespace
				if( wsString && wsString.index == 0 ) { // spaces and tabs
					newToken = new CSSWhitepaceToken().setNumSpaces( wsString[1].length );
				} else if( nlString && nlString.index == 0 ) { // newlines
					newToken = new CSSNewLineToken().setNumSpaces( nlString[1].length );
				}
				/**
				 * increment i needs to be made the last index of the match
				 * 
				 * eg. String is `(`
				 *     i = 0, match is `(`
				 *     0 is first index of the match
				 *     0 is last index of the match
				 * 
				 * 
				 * eg. String is `the quick brown fox`
				 *     i = 0, match is `t`
				 *     i = 1, match is `h`
				 *     i = 2, match is `e`
				 *     0 is first index of the match => `the`
				 *     2 is last index of the match  => `the`
				 *     
				 *     i=0; `the`.length = 3
				 *     i += length => 3, match is ` `
				 *     3 is first index of the next match
				 *     
				 *
				 * index + length     = next character
				 * index + length - 1 = last character of the current match
				 */
			} else if( cn == CSSCommentOpenToken.ident ) {
				newToken = new CSSCommentOpenToken();
			} else if( cn == CSSCommentCloseToken.ident ) {
				newToken = new CSSCommentCloseToken();
			} else if( cdoString && cdoString.index == 0 ) {
				newToken = new CSSCDATAOpenToken( cdoString[1] );
			} else if( cdcString && cdcString.index == 0 ) {
				newToken = new CSSCDATACloseToken( cdcString[1] );
			} else if( CSSMatcherToken.matcherSymbol == n && [ 
						CSSPrefixMatcherToken.prefix, 
						CSSSuffixMatcherToken.prefix, 
						CSSSubstringMatcherToken.prefix, 
						CSSDashMatcherToken.prefix, 
						CSSIncludeMatcherToken.prefix 
					].includes( c ) 
				) {
				switch( cn ) {
					case CSSPrefixMatcherToken.ident    : newToken = new CSSPrefixMatcherToken();    break;
					case CSSSuffixMatcherToken.ident    : newToken = new CSSSuffixMatcherToken();    break;
					case CSSSubstringMatcherToken.ident : newToken = new CSSSubstringMatcherToken(); break;
					case CSSDashMatcherToken.ident      : newToken = new CSSDashMatcherToken();      break;
					case CSSIncludeMatcherToken.ident   : newToken = new CSSIncludeMatcherToken();   break;
				}
			} else if( escapeString && escapeString.index == 0 ) {
				newToken = new CSSEscapeToken( escapeString[1] );
			} else if( unicodeString && unicodeString.index == 0 ) {
				newToken = new CSSUnicodeRangeToken( unicodeString[1] );
			} else if( numString && numString.index == 0 ) {
				newToken = new CSSNumberToken( numString[1] );
			} else if( c == CSSHashToken.prefix && hashIdentString && hashIdentString.index == 0 ) {
				newToken = new CSSHashToken( hashIdentString[1] );
			} else if( c == CSSAtKeywordToken.prefix && identString && identString.index == 1 ) {
				newToken = new CSSAtKeywordToken( identString[1] );
			} else if( identString && identString.index == 0 ) {
				newToken = new CSSIdentToken( identString[1] );
			} else {
				switch( c ) {
					case CSSParenOpenToken.ident        : newToken = new CSSParenOpenToken();        break;
					case CSSParenCloseToken.ident       : newToken = new CSSParenCloseToken();       break;
					case CSSBraceOpenToken.ident        : newToken = new CSSBraceOpenToken();        break;
					case CSSBraceCloseToken.ident       : newToken = new CSSBraceCloseToken();       break;
					case CSSBracketOpenToken.ident      : newToken = new CSSBracketOpenToken();      break;
					case CSSBracketCloseToken.ident     : newToken = new CSSBracketCloseToken();     break;
					case CSSApostropheToken.ident       : newToken = new CSSApostropheToken();       break;
					case CSSQuoteToken.ident            : newToken = new CSSQuoteToken();            break;
					case CSSColonToken.ident            : newToken = new CSSColonToken();            break;
					case CSSSemicolonToken.ident        : newToken = new CSSSemicolonToken();        break;
					case CSSCommaToken.ident            : newToken = new CSSCommaToken();            break;
					case CSSPercentToken.ident          : newToken = new CSSPercentToken();          break;
					case CSSExclamationPointToken.ident : newToken = new CSSExclamationPointToken(); break;
					default                             : newToken = new CSSDelimiterToken( c );
				}
			}

			i += newToken.length - 1;
			tokens.push( newToken );
		}
		tokens.push( new CSSEOFToken() );
		if( rawOnly )
			return tokens;

		/*** PASS 2 - Token Clumps ***/
		let i=0;
		while( i < tokens.length ) {
			let c = tokens[i];
			let n = tokens[i+1];

			let newToken;

			/*** Clumping: https://www.w3.org/TR/css-syntax-3/#consume-a-numeric-token ***/
			if( c.constructor.name == `CSSNumberToken` && n.constructor.name == `CSSIdentToken` ) { 
				newToken = new CSSDimensionToken( c, n ); 
			} else if( c.constructor.name == `CSSNumberToken` && n.constructor.name == `CSSPercentToken` ) { 
				newToken = new CSSPercentageToken( c, n ); 
			} else if( [ `CSSQuoteToken`, `CSSApostropheToken`].includes( c.constructor.name ) ) { 
				let closer = tokens.map( e=>e.val ).indexOf(c.val, i+1);
				if( closer == -1 ) { closer = tokens.length - 1; }
				newToken = new CSSStringToken( ...tokens.slice( i, closer + 1 ) );
			} else if( c.val == CSSExclamationPointToken.ident ) {
				let closer = tokens.map( e=>e.val ).indexOf( `important`, i );
				/* https://www.w3.org/TR/css-syntax-3/#%21important-diagram */
				if( tokens.slice( i+1, closer+1 ).filter( e=>{ 
					/**
					* if there is anything between the exclamation point and `important` besides whitespace, 
					* don't create an important token 
					*/
					return ( !( e instanceof CSSWhitepaceToken ) && e.val != 'important' );
				}).length == 0 ) {
					while( tokens[closer+1] instanceof CSSWhitepaceToken ) {
						closer++;
					}
					newToken = new CSSImportantToken( ...tokens.slice( i, closer + 1 ) );
				}
			}


			if( newToken ) {
				tokens.splice( i, newToken.length, newToken );
			}
			i++;
		}

		/*** PASS 3 - Grouping: https://www.w3.org/TR/css-syntax-3/#parser-diagrams ***/
		tokens = CSSParser.group( tokens );
// TODO(andrew):	urlUnquoted
// TODO(andrew):	declaration

		return tokens;
	}
}
CSSParser.GroupTypes = [
	CSSBraceBlockToken,
	CSSBracketBlockToken,
	CSSParenBlockToken,
	CSSCDATAToken,
	CSSCommentToken
];